module gitlab.com/inquizarus/eawsc

go 1.14

require (
	github.com/gosidekick/goconfig v1.3.0
	gopkg.in/ini.v1 v1.55.0
)
