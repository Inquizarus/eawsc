# ExportAWSCredentials
Simple CLI utility to generate `export` statements based on a profile inside an AWS credentials file.

## Example
Below we use the golden file (usually used when running unit tests) to generate export statements for the default `saml` profile.
```bash
$ ./eawsc -credentials_file ./example.golden
export AWS_ACCESS_KEY_ID=123TheAccessKeyID
export AWS_SECRET_ACCESS_KEY=123TheSecretAccessKey
export AWS_SESSION_TOKEN=123TheSessionToken
```

If there are multiple profiles one can easily switch to create export statements for another one by using the `-profile` flag.
```bash
$ ./eawsc -credentials_file ./example.golden -profile default
export AWS_ACCESS_KEY_ID=321TheAccessKeyID
export AWS_SECRET_ACCESS_KEY=321TheSecretAccessKey
export AWS_SESSION_TOKEN=321TheSessionToken
```


## Help section

```bash
$ ./eawsc -h
Usage
  -credentials_file string
    
  -exportpattern string
         (default "export AWS_ACCESS_KEY_ID=%s\nexport AWS_SECRET_ACCESS_KEY=%s\nexport AWS_SESSION_TOKEN=%s")
  -profile string
         (default "saml")
Environment variables:
  $PROFILE string
        (default "saml")
  $CREDENTIALS_FILE string

  $EXPORTPATTERN string
        (default "export AWS_ACCESS_KEY_ID=%s\nexport AWS_SECRET_ACCESS_KEY=%s\nexport AWS_SESSION_TOKEN=%s")
```

## Download latest releases

[regular](https://gitlab.com/Inquizarus/eawsc/-/jobs/artifacts/master/raw/builds/eawsc?job=compile)

[amd64](https://gitlab.com/Inquizarus/eawsc/-/jobs/artifacts/master/raw/builds/eawsc_amd64?job=compile)

[arm6](https://gitlab.com/Inquizarus/eawsc/-/jobs/artifacts/master/raw/builds/eawsc_arm6?job=compile)