package main

type credentialsConfig struct {
	File string `cfgRequired:"true"`
}

// Config for eawsc
type Config struct {
	Profile       string `cfgDefault:"saml"`
	Credentials   credentialsConfig
	ExportPattern string `cfgDefault:"export AWS_ACCESS_KEY_ID=%s\nexport AWS_SECRET_ACCESS_KEY=%s\nexport AWS_SESSION_TOKEN=%s"`
}
