package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"github.com/gosidekick/goconfig"
	ini "gopkg.in/ini.v1"
)

const (
	appName = "eawsc"
)

// Credential struct for holding aws credential configurations
type Credential struct {
	Name           string
	AccessKeyID    string
	AccessKeyToken string
	SessionToken   string
}

func execute(c Config) {
	fc, err := ioutil.ReadFile(c.Credentials.File)
	check(err)
	r := bytes.NewReader(fc)
	credentials, err := parseAwsCredentials(r)
	check(err)
	credential, err := getCredentialsByName(c.Profile, credentials)
	check(err)
	fmt.Println(generateExportStringForCredentials(credential, c))
}

func main() {
	config := Config{}
	check(goconfig.Parse(&config))
	execute(config)
}

func parseAwsCredentials(r io.Reader) ([]Credential, error) {
	c, err := ioutil.ReadAll(r)
	check(err)
	cfg, err := ini.Load(c)
	check(err)
	creds := []Credential{}
	for _, section := range cfg.Sections() {
		cred, err := transformIniSectionIntoCredential(section)
		if nil != err {
			continue
		}
		creds = append(creds, cred)
	}

	return creds, nil
}

func transformIniSectionIntoCredential(section *ini.Section) (Credential, error) {
	ID, err := section.GetKey("aws_access_key_id")
	Secret, err := section.GetKey("aws_secret_access_key")
	Token, err := section.GetKey("aws_session_token")
	if nil != err {
		return Credential{}, fmt.Errorf("section did not contain all the expected information")
	}
	cred := Credential{
		Name:           section.Name(),
		AccessKeyID:    ID.String(),
		AccessKeyToken: Secret.String(),
		SessionToken:   Token.String(),
	}
	return cred, nil
}

func getCredentialsByName(name string, credentials []Credential) (Credential, error) {
	for _, credential := range credentials {
		if name == credential.Name {
			return credential, nil
		}
	}
	return Credential{}, fmt.Errorf("could not find credentials with name %s in collection", name)
}

func generateExportStringForCredentials(credential Credential, c Config) string {
	return fmt.Sprintf(c.ExportPattern, credential.AccessKeyID, credential.AccessKeyToken, credential.SessionToken)
}

func check(err error) {
	if nil != err {
		fmt.Println(err)
		os.Exit(1)
	}
}
