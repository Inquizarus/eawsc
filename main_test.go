package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"testing"

	"github.com/gosidekick/goconfig"
)

func TestThatCredentialsCanBeParsed(t *testing.T) {
	fc, _ := ioutil.ReadFile("example.golden")
	r := bytes.NewReader(fc)
	creds, _ := parseAwsCredentials(r)
	if 1 > len(creds) {
		t.Errorf("wrong amount of creds parsed, got %d but wanted %d", len(creds), 1)
	}
}

func TestThatCredCanBeRetrievedByName(t *testing.T) {
	creds := []Credential{
		{
			Name:           "saml",
			AccessKeyID:    "123AccessKeyID",
			AccessKeyToken: "123AccessKeyToken",
			SessionToken:   "123SessionToken",
		},
	}
	_, err := getCredentialsByName("saml", creds)
	if nil != err {
		t.Errorf("could not retrieve credentials with name %s from credential collection", "saml")
	}
}

func TestThatAnErrorIsReturnedWhenCredentialCannotBeFoundByName(t *testing.T) {
	creds := []Credential{}
	_, err := getCredentialsByName("saml", creds)
	if nil == err {
		t.Error("an error where not returned when expected since credentials could not be found")
	}
}

func TestThatItGeneratesTheCorrectExportStringForCredentials(t *testing.T) {
	credential := Credential{
		Name:           "saml",
		AccessKeyID:    "123AccessKeyID",
		AccessKeyToken: "123AccessKeyToken",
		SessionToken:   "123SessionToken",
	}
	config := Config{}
	goconfig.Parse(&config)
	expected := fmt.Sprintf("export AWS_ACCESS_KEY_ID=%s\nexport AWS_SECRET_ACCESS_KEY=%s\nexport AWS_SESSION_TOKEN=%s", credential.AccessKeyID, credential.AccessKeyToken, credential.SessionToken)

	actual := generateExportStringForCredentials(credential, config)

	if expected != actual {
		t.Errorf("did not generate expected export string, got %s but expected %s", actual, expected)
	}
}
